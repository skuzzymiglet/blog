---
title: "Ras - a Conlang I made"
date: 2020-03-30T09:41:18+01:00
draft: true
---

Totally off-topic post. Let me present my conlang - Ras. I started it in December 2019. It's the language of thenomadic Ras people of my (built) world.

## Phonetics

+ **Plosives**: k, g (p, b found in mutation), ʔ
+ **Fricatives**: θ, ð, f, v, h, ɮ, s, z
+ **Approximants**: ɹ 
+ **Vowels**: ɑ, ə, ɪ, ʌ, o (and nasalized forms)

Yep, it's weird but it feels nice to speak in my oppinion

## Phonotactics

Simple and Toki Pona-like:

+ No consective vowels/consonants
+ Separate vowels with a glottal stop 

## Grammar

### Classes

|Number|Type|Example (English)|Ras|Vowel Template|
|-|-|-|-|-|
|1|Future, distant concepts|Doom|vɪzɪ|-ɪ-ɪ-
|2|Present feelings|Regret|vɑhɪ|-ɑ-ɪ-
|3|Strong present feelings|Substance-induced Euphoria|kʌkɪso|-ʌ-ɪ-
|4|Non-sentimental technologies|Stick|ɹəʔoʔ|-ə-
|5|Sentimental items|Written Text|kaðʌ|-ɑ-|
|6|Occupations|Farmer|võfõ|-õ-
|7|Land features|Mountain|ɹɪko|-ɪ-o-|
|8|Non-animal organism|Bush|hɪsɑi|-ɪ-
|9|Animal|Bird|ɮʌkɪs|-ʌ-
|10|Friends, family, groups of people|Mother|ho|-o-

+ Numbers are just templates and they're filled with the vowels of the noun they count **or** with -ɪ-ɪ- for other purposes

### Verb tense, aspect, mood and voice

|First consonant|Past|Present|Future|Other|Habituality|Perfectivity|Mood|Voice|
|-|-|-|-|-|-|-|-|-|
|s|weak|strong||effect|||realis|passive|
|z|strong|weak|||strong|strong|weak irrealis||
|θ|strong||||strong||realis|passive
|ð||strong|weak|cause|||realis||
|k|||strong||||realis|passive|
|g|weak||strong|past-in-the-future|||irrealis|
|ɮ|||||||irrealis
|ɹ|weak||||strong||irrealis

### Mood

The vowel afterwards indicates mood:

|Vowel|Mood|
|-|-|
|ɪ|default|
|ɑ|subjunctive|
|ə|anti-subjunctive|
|ʌ|conditional|
|o|obligation|

### Person

independent/suffix

||1|2|3
|-|-|-|-
|singular|ɑkɪ/fɑ|ɑko/ko|ɪkɑ/ðo
|plural|ɑkɪɹ/fɪ|ɑkoɹ/koɹ|ɪkɑɹ/ðʌ

### Ergativity

I run: run(1st person singular)
I hate you: I hate(2nd person singular)

## Examples

English|Ras
-|-
I run|

### Adjective types

|Number|Type|Ending|Example (English)|Ras|
|-|-|-|-|-|
|1|Physical|-ɪ_|Short|kɪθ|
|2|Non-physical|-ɑ_|Intelligent|ɹɑzɪsɪʔɑz|
|3|Subjective|-ək|Smart (in my opinion)|ɹɑzɪsɪʔək|
|4|Verbal|-ʌ_|Running|zʌkʌʔʌk |


### Ergativity
