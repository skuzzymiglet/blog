---
title: "One Year of Vim"
date: 2020-02-21T18:05:41Z
draft: true
---

# One year of Vim - my experience

1 year ago I switched to Vim. After getting tired of IDLE, the "disposable" Python editor, I went on a search for something better. After trying KDE Kate, and generally looking for everything trying to make it look like IDLE, I searched for an "emacs for python" tutorial.

It didn't work, so I did the natural thing and searched for "vim for python". It recommended [fisa-vim-config](http://vim.fisadev.com/), so I did as I was told and installed it.

Good, but over time I found it hard to tweak, as I knew zero vim previously. My config got messy as I sought out some words and replaced them with others to change the color scheme. I knew Vim basics, but I couldn't configure.

But then it got a whole lot better.

I was using KDE Plasma, but had heard of i3. So I installed it on an old HP laptop I had lying around to rip CDs. I set up vim and bash and it was great. I built something from the ground up, something minimal, not just copied off the internet, and learnt from a [cheatsheet](https://vim.rtorr.com/), making some macros (admittedly for stuff that could be done with Visual Block). I soon copied the bash aliases I made to KDE. 

From that, it was a process of learning vim tricks and plugins. Here are some of Vim's best features in my opinion:

# The built-in stuff


