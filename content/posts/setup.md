---
title: "My Setup"
date: 2020-03-14T08:14:27Z
draft: false
---

![setup](/img/screenshot-24_03_20_018.png)
![setup 2](/img/screenshot-25_03_20_000.png)

An update on my setup. I haven't shown it since last October and you people seem to like it.

## OS: Arch Linux

I love how you get to build your system fully. Yet the rolling release model means you can install and forget, with no worries about full-system upgrades like you do in Ubuntu.

## WM: bspwm (switched from i3)

I love bspwm's binary tree tiling. It's slightly harder to set up the i3, but I recommend it to anyone with a bit of time.

## Browser: qutebrowser

I fell in love with this browser straight after I installed it because previously I used Firefox which required me to use my mouse. Even though I installed a vim keys plugin, I didn't use the keys. qutebrowser encouraged e to do so. 

## Text editor: vim

In a manageable 78 lines of vimrc, I've got a multi-purpose text-editor for writing, programming and accidentaly opening binary files in `vifm`. It just accomodates everything. Also vim-like software is nice - for example qutebrowser

## Bar: polybar

I don't really like polybar. 415 lines of config is excessive and the bar doesn't look that good. It's not even themeable. In the i3 days, I used [`bumblebee status`](https://github.com/tobi-wan-kenobi/bumblebee-status/) and it was fun. Just a command, themes, i3 integration. I'm looking for another bar. I may hop to dwm and use dwmbar or use [yabar](https://github.com/tobi-wan-kenobi/bumblebee-status/)

## File manager: vifm

vifm is a good, fast, vim-like file manager. I haven't had many problems with it. Its 512 lines of config are excessive, but they do include comments. Because of the builtin file-opening, my xdg-open command is pretty stupid. I may switch to `nnn`. I also tried out [dmenufm](https://github.com/huijunchen9260/dmenufm). I'll decide sometime

## Email client: neomutt

I set this up with [mutt-wizard](https://github.com/LukeSmithxyz/mutt-wizard). It's email - I don't really care. It works.

## Matrix client: gomuks

I discovered this a few days ago. It's a good matrix client, but doesn't support everything Riot.im does. But I look forward to contributing to it on GitHub

## System monitor: ytop

`gotop` was really good and the migration to `ytop` was easy. It's fast and visual, and I'll keep it

# TODO

+ I may distro-hop to Void and try out dwm
+ [youtube-viewer](https://github.com/trizen/youtube-viewer) setup
+ Lilypond + vim + git composition setup - I'll write an article
+ More qutebrowser shortcuts
+ ctrlp.vim config - tags are really slow right now
+ Making these dotfiles portable and installing them on another computer

Happy ricing!
