---
title: "Termdown and shutdown: get off the computer"
date: 2020-03-07T20:42:37Z
draft: true
---

You've spent hours on the computer and need to get something done. How will you do it?

For me, the ultimate way to do this is:

`td 1m;sd`

+ td: 
