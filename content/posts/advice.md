---
title: "5 tips"
date: 2020-03-08T09:48:25Z
draft: false
---

Just 5 general tips for programming (and other things)

# Do it now

If you want to do something, just do it. Don't get used to a bug or error. Don't adjust to multiple keypresses. Look it up on GitHub, Stackoverflow or just Google it. It's rewarding fixing things

I struggle to adhere to this sometimes. But it's a rule that pays off

# Use it

If you make somthing, you should find a use for it yourself. If not, it's a bit pointless. For example, I started making an animation program in Go even though I've never done animation and probably never will. And not only was the code dysfunctional, the interface was unintuitive. If you'll use something, it'll help you make it easy to use

The exception is code excersises and the like, but they should not take long-term effort

# Research

Research for similar things. There's no worse feeling than seeing something exactly the same as yours. Even if it seems unique, look at things to find the strong points of your idea

# Learn lightly

Don't spend months learning simple things. With so much resources online, you don't need to memorize everything. Don't go for heavy tutorials (e.g. books over 100 pages), do something light. And make sure to do practical things, but just do the essentials.


Good, light learning resources in my opinion:

+ [A Tour of Go](https://tour.golang.org/) - fast-paced and practical
+ [Vue Essentials](https://vuejs.org/v2/guide/index.html) - the introduction is packed and then there are tidy sections

To polish up on skills, go for an in-depth resource like [Serious Python](https://serious-python.com/) which expands into style, testing, packaging, optimization and even databases.

# Have fun

Have fun while programming! If it's boring or tedious, there's probably a solution. Just have an end goal in mind!
