---
title: "Dotfiles"
date: 2020-04-11T19:12:07+01:00
draft: false
---

My dotfiles are my most active repo on GitHub - unsurprisingly, as I change them at least once a week, and that they'll stick around much longer than the small projects. But they're not very useful to anyone other than me, here's why:

+ My username is hardcoded (mostly fixed)
+ Mountpoint aliases even I don't use
+ Hardcoded network devices
+ Pretty hard to navigate (except, perhaps, for my vimrc)
+ No way to install - ideally I'd use [LARBS](https://github.com/LukeSmithxyz/LARBS) but I need to make a program list

Unsurprisingly, trying to install my dotfiles on friends' machines, it was dirty. I had a half-assed install script, but the problem was probably the abysmal hard-coding.

The best start for a program's config is something like vim's - zero config yields a viable text editor, and there's no wizard or something like that that creates a deep mess of comments, or even just puts it in by default. Config files should not be over 150 lines, ideally.

# Porting

I will try to port my dotfiles to a 10-year-old, 32-bit [HP ProBook 4720s](https://support.hp.com/us-en/document/c02057767)

![ProBook](https://support.hp.com/doc-images/800/c02104648.gif)

Currently it's running Debian with default i3 (it's pretty usgle)

Here's some tentative tips for porting:

+ Different usernames - no one really wants your username in their configs - I'm `chunkymiglet` on the ProBook
+ Meh, just different things - keyboard layouts, multiple screens (which I don't have)
+ Try different architechtures - 32 bit is going soon but well, it's a nice challenge

That's about it
