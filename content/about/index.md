---
title: "About me"
date: 2020-03-01T09:33:22Z
draft: false
---

I do programming as a hobby. I know Python well, learnt Go in October, I know basic JS (and obviously HTML and CSS) and I'm learning Vue. I have many projects to do!
