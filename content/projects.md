---
title: "Projects"
date: 2020-03-01T09:39:54Z
draft: false
---

A list of my todo, in-progress and done projects

# Done

+ [asl](https://github.com/skuzzymiglet/asl) - a tool to make timelapses of your screen, replaced [asl.sh](https://github.com/skuzzymiglet/asl.sh), a 2-line script, but now I don't use it because it's intrinsically too heavy
+ [animator](https://github.com/skuzzymiglet/animator), a command-line animation tool I made in Go. It's kind of stupid, as I knew nothing about animation, so the interface is horrible and the code is functionally flawed
+ [point](https://github.com/skuzzymiglet/point) - a simple, fast, tool to create HTML presentation you can open anywhere

# In-progress

Not much i guess 😃

# Todo

(no particular order by the way)

+ act - a fast, scriptable activity monitor using a performant timeseries DB (written in Go)
+ songit - a sensible Git sonification tool 
+ Termox - TUI client for [tox](https://github.com/irungentoo/toxcore), a peer to peer communication platform 
+ midic - customizable keyboard MIDI controller
+ lyt.vim - a MuseScore-like timeline for Lilypond in Vim ![MuseScore Timeline](/img/handbooktimeline.PNG)
+ Chatstats - a Matrix/Discord/Slack chat statistics bot (to determine whether the server is _dying_)
+ RealDB - a database of Jazz tunes for applications to use
+ Really - free, open source app that plays the (Jazz) beat/chords/melody for you (alternative to iRealPro)
+ Infinite Aural - pulls and ranks melodies from [IMSLP](https://imslp.org/) and makes [ABRSM aural tests](https://gb.abrsm.org/en/our-exams/what-is-a-graded-music-exam/aural-tests/)
